using UnityEngine;
using System.Collections;

public class FireCtrl : MonoBehaviour {

	[SerializeField]
	int sortingOrder = -1;

	// Use this for initialization
	void Start () {
		GetComponent<Renderer>().sortingOrder = sortingOrder;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
