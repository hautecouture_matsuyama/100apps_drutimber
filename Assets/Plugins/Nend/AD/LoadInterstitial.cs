﻿using UnityEngine;
using System.Collections;

using NendUnityPlugin.AD;
using NendUnityPlugin.Common;

public class LoadInterstitial : MonoBehaviour {

    private bool flg;

    // Use this for initialization
    void Start()
    {
        // attach EventHandler
        NendAdInterstitial.Instance.AdLoaded += OnFinishLoadInterstitialAd;
        LoadAd();
    }
    /*
    // Update is called once per frame
    void Update()
    {
        if (Application.platform == RuntimePlatform.Android && Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
    */
    void OnDestroy()
    {
        // detach EventHandler
        NendAdInterstitial.Instance.AdLoaded -= OnFinishLoadInterstitialAd;
    }

    private void LoadAd()
    {
        string apiKey = "8c278673ac6f676dae60a1f56d16dad122e23516";
        string spotId = "213206";

#if UNITY_IPHONE
        apiKey = "33b71b1937bb59c1dcb333212bf02a55b35006bb";
        spotId = "370206";
		Handheld.SetActivityIndicatorStyle(iOSActivityIndicatorStyle.Gray);
#elif UNITY_ANDROID
        apiKey = "8c278673ac6f676dae60a1f56d16dad122e23516";
        spotId = "213206";
        Handheld.SetActivityIndicatorStyle(AndroidActivityIndicatorStyle.Small);
#endif

        Handheld.StartActivityIndicator();

        NendAdInterstitial.Instance.Load(apiKey, spotId);
    }

    private void ReloadAd()
    {
        StartCoroutine(LoadAdDelay());
    }

    private IEnumerator LoadAdDelay()
    {
        yield return new WaitForSeconds(2.0f);
        LoadAd();
    }

    public void OnFinishLoadInterstitialAd(object sender, NendAdInterstitialLoadEventArgs args)
    {
        Handheld.StopActivityIndicator();

        NendAdInterstitialStatusCode statusCode = args.StatusCode;
        switch (statusCode)
        {
            case NendAdInterstitialStatusCode.SUCCESS:
                // Move to the next scene when Interstitial-AD load completed.
                if (flg) return;
                //NendAdInterstitial.Instance.Show();	testで消してみる
                flg = true;
                break;
            default:
                // When failed to get of Interstitial-AD, try to reload.
                ReloadAd();
                break;
        }
    }
}
