﻿using UnityEngine;
using System.Collections;
using NendUnityPlugin.AD;
using NendUnityPlugin.Common;

public class NendBannerManager : MonoBehaviour
{

    private NendAdBanner banner;
    
    
    [SerializeField]
    private string bannerObjctName;

    [SerializeField]
    private bool hideOnSceneChanged;

    private const string defaultBannerObjName = "NendAdBanner";

    private int showedLevel = -1;

    //prepare set new gravities.
    [System.Serializable]
    public class GravityCandidates
    {
        public Gravity[] gravities;
    }
    [SerializeField]
    private GravityCandidates[] gravityCandidates;

    public GravityCandidates GetGravityCandidate(int index)
    {
        return gravityCandidates[index];
    }

    //ManagerCache
    static NendBannerManager s_manager;

    public static NendBannerManager GetManager()
    {
        if (s_manager == null)
        {
            NendBannerManager obj = Component.FindObjectOfType(typeof(NendBannerManager)) as NendBannerManager;
            if (obj)
            {
                s_manager = obj;
            }
            else
            {
                GameObject go = new GameObject("NendAdManager");
                obj = go.AddComponent<NendBannerManager>() as NendBannerManager;
                s_manager = obj;
            }
        }
        return s_manager;
    }

    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    public void ShowAd(string adName = defaultBannerObjName)
    {
        CheckAdInstance(adName);

        showedLevel = Application.loadedLevel;
        banner.Show();
    }

    public void ChangeAdPosition(Gravity[] newGravity, string adName = defaultBannerObjName)
    {
        CheckAdInstance(adName);
        
        banner.ChangeGravity(newGravity);
    }

    //Check if instance exist.
    void CheckAdInstance(string adName)
    {
        if (banner == null)
        {
            banner = NendUtils.GetBannerComponent(adName);
        }
    }

    void OnLevelWasLoaded(int level)
    {
        if (!hideOnSceneChanged) return;
        if (level != showedLevel) return;

        showedLevel = -1;
        HideAd();
    }

    public void HideAd()
    {
        if(banner != null)
            banner.Hide();
    }
}
