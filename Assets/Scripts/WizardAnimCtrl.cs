﻿using UnityEngine;
using System.Collections;

public class WizardAnimCtrl : BaseEnemyCtrl {

	[SerializeField]
	GameObject magicPrefab;
	[SerializeField]
	float distFromCenter;

	// Use this for initialization
	void Start () {
		base.Start ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void Attack(int dir)
	{
		GameObject goMagic = 
			Instantiate(magicPrefab, 
			            transform.position + dir * distFromCenter * Vector3.right, 
			            Quaternion.identity) as GameObject;

		MagicMotion magicMotion = goMagic.GetComponent<MagicMotion>();

		magicMotion.Shot (dir);
	}
	public void AttackRight()
	{
		Attack (1);
	}
	public void AttackLeft()
	{
		Attack (-1);
	}
}
