﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour {

	//public UISlider healthslider;

	public float health;
	public float healthForLabel;

	[SerializeField]
	private float initHealthupdateVar = 0.2f;

	[SerializeField]
	private int maxLevelUpCnt = 40;

	[SerializeField]
	private float speedUpVal = 0.02f;

	[SerializeField]
	private float incrementVal = 0.3f;

	//[SerializeField]
	//private GameObject timeOverObject;

	[SerializeField]
	private bool timeInfinite = false;

	float initHealthForLabel;

	private float HealthupdateVar = 0.2f;

	private int preLevel = 0;

	private RectTransform rctTrfm
	{
		get{
			if(_rctTrfm == null){
				_rctTrfm = gameObject.GetComponent<RectTransform>();
			}
			return _rctTrfm;
		}
	}
	private RectTransform _rctTrfm;

	private PlayerController player
	{
		get{
			if(_player == null){
				_player = GameObject.Find("Player")
					.GetComponent<PlayerController>();
			}
			
			return _player;
		}
	}
	private PlayerController _player;

	Animator HPAnimCtrl{
		get{
			if(_HPAnimCtrl == null){
				_HPAnimCtrl = gameObject.GetComponent<Animator>();
			}
			return _HPAnimCtrl;
		}
	}Animator _HPAnimCtrl;

	private Soundmanager smanager
	{
		get{
			if(_smanager == null){
				_smanager = 
					GameObject.Find("Audiomanager").GetComponent<Soundmanager>();
			}
			
			return _smanager;
		}
	}
	private Soundmanager _smanager;

	public void UpdateHealth() {
		//Debug.Log("Updating health");

		if(health > healthForLabel)
		{
			healthForLabel = health;
		}

		if(health > 0) {
			//healthslider.sliderValue = health / healthForLabel;
			rctTrfm.localScale = new Vector3(
				health / healthForLabel, 
				rctTrfm.localScale.y, 
				rctTrfm.localScale.z);
		}else if( health <=0) {
			//timeOverObject.SetActive(true);
			player.GameOver(1);

			//healthslider.sliderValue = health / healthForLabel;

		}
	}


	// Use this for initialization
	void Start () {
		initHealthForLabel = healthForLabel;

		HealthupdateVar = //1.7f
			initHealthupdateVar;
		//healthslider.sliderValue = 1;
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		if(!timeInfinite){

			if(GameManagerScript.potionFlag == false){
				health -= Time.fixedDeltaTime * HealthupdateVar;
			}else{
				if(health + Time.fixedDeltaTime * HealthupdateVar > healthForLabel)
				{
					health = healthForLabel;

					/*
					GameManagerScript.potionFlag = false;

					player.protectorAnim.SetTrigger("Reset");

					HPAnimCtrl.SetTrigger("Reset");

					player.protectorTrail.enabled = false;

					smanager.StopMutekiSound();
					smanager.PlayMusic();
					*/
				}
				else
				{
					health += Time.fixedDeltaTime * HealthupdateVar;
				}
			}
		}

		UpdateHealth();
	}

	public void HealthBarSpeedUp()
	{
		if(preLevel < maxLevelUpCnt){
			HealthupdateVar += speedUpVal;

			preLevel++;

			player.SetMinTweenTime(preLevel, maxLevelUpCnt);
		}

	}

	public void IncreaseHealthBar()
	{
		if (GameManagerScript.potionFlag == false) {
			health = Mathf.Min (health + incrementVal, healthForLabel);
		}
		//health += incrementVal;
	}
}
