﻿using UnityEngine;
using System.Collections;

public class GilAnimCtrlScript : MonoBehaviour {

	[SerializeField]
	float walkSpeed = 1f;
	[SerializeField]
	float idleMinTime, idleMaxTime, walkMinTime, walkMaxTime, readyMinTime, readyMaxTime;

	Animator animator{
		get{
			if(_animator == null){
				_animator = gameObject.GetComponent<Animator>();
			}
			return _animator;
		}
	}Animator _animator;

	GilParentScript gilParent{
		get{
			if(_gilParent == null){
				_gilParent = transform.parent.GetComponent<GilParentScript>();
			}
			return _gilParent;
		}
	}GilParentScript _gilParent;

	Soundmanager soundmanager{
		get{
			if(_soundmanager == null){
				_soundmanager = GameObject.Find("Audiomanager").GetComponent<Soundmanager>();
			}
			return _soundmanager;
		}
	}Soundmanager _soundmanager;

	// Use this for initialization
	void Start () {

		StartCoroutine (AnimationCrtn());
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator AnimationCrtn()
	{
		yield return new WaitForSeconds(Random.Range(idleMinTime, idleMaxTime));

		while(true)
		{

			AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);

			int stateCount,rnd;
			string trigger;
			float waitTime = Random.Range(idleMinTime, idleMaxTime);

			if(stateInfo.nameHash == Animator.StringToHash ("Base Layer.IdleShieldLeft") || 
			   stateInfo.nameHash == Animator.StringToHash ("Base Layer.SetSwordLeft")){
				stateCount = 4;
				rnd = Random.Range(0,stateCount);

				if(gilParent.rightForced){
					trigger = "TurnRight";
				}else if(rnd == 0 && gilParent.leftForced == false){
					trigger = "TurnRight";
				}else if(rnd == 0 || rnd == 1){
					trigger = "Walk";
					WalkToLeft();
					waitTime = Random.Range(walkMinTime, walkMaxTime);
				}else{
					trigger = "ReadySword";
					waitTime = Random.Range(readyMinTime, readyMaxTime);
				}
				animator.SetTrigger(trigger);
			}else if(stateInfo.nameHash == Animator.StringToHash ("Base Layer.IdleShieldRight") || 
			         stateInfo.nameHash == Animator.StringToHash ("Base Layer.SetSwordRight")){
				stateCount = 4;
				rnd = Random.Range(0,stateCount);

				if(gilParent.leftForced){
					trigger = "TurnLeft";
				}
				else if(rnd == 0 && gilParent.rightForced == false){
					trigger = "TurnLeft";
				}
				else if(rnd == 0 || rnd == 1){
					trigger = "Walk";
					WalkToRight();
					waitTime = Random.Range(walkMinTime, walkMaxTime);
				}else{
					trigger = "ReadySword";
					waitTime = Random.Range(readyMinTime, readyMaxTime);
				}
				animator.SetTrigger(trigger);
			}else if(stateInfo.nameHash == Animator.StringToHash ("Base Layer.IdleSwordLeft") || 
			         stateInfo.nameHash == Animator.StringToHash ("Base Layer.ReadySwordLeft")){
				stateCount = 3;
				rnd = Random.Range(0,stateCount);

				if(gilParent.rightForced){
					trigger = "TurnRight";
				}else if(rnd == 0 && gilParent.leftForced == false){
					trigger = "TurnRight";
				}else if(rnd == 0 || rnd == 1){
					trigger = "Walk";
					WalkToLeft();
					waitTime = Random.Range(walkMinTime, walkMaxTime);
				}else{
					trigger = "SetSword";
					waitTime = Random.Range(readyMinTime, readyMaxTime);
				}
				animator.SetTrigger(trigger);
			}else if(stateInfo.nameHash == Animator.StringToHash ("Base Layer.IdleSwordRight") || 
			         stateInfo.nameHash == Animator.StringToHash ("Base Layer.ReadySwordRight")){
				stateCount = 3;
				rnd = Random.Range(0,stateCount);

				if(gilParent.leftForced){
					trigger = "TurnLeft";
				}else if(rnd == 0 && gilParent.rightForced == false){
					trigger = "TurnLeft";
				}else if(rnd == 0 || rnd == 1){
					trigger = "Walk";
					waitTime = Random.Range(walkMinTime, walkMaxTime);
					WalkToRight();
				}else{
					trigger = "SetSword";
					waitTime = Random.Range(readyMinTime, readyMaxTime);
				}
				animator.SetTrigger(trigger);
			}else if(stateInfo.nameHash == Animator.StringToHash ("Base Layer.WalkShieldLeft") || 
			         stateInfo.nameHash == Animator.StringToHash ("Base Layer.WalkShieldRight") || 
			         stateInfo.nameHash == Animator.StringToHash ("Base Layer.WalkSwordLeft") || 
			         stateInfo.nameHash == Animator.StringToHash ("Base Layer.WalkSwordRight")){
				animator.SetTrigger("Stop");
				StopWalk();

			}

			yield return new WaitForSeconds(waitTime);
		}
	}

	void WalkToLeft()
	{
		transform.parent.GetComponent<Rigidbody2D>().velocity = Vector2.right * (-walkSpeed); 
	}

	void WalkToRight()
	{
		transform.parent.GetComponent<Rigidbody2D>().velocity = Vector2.right * (+walkSpeed); 
	}

	void StopWalk()
	{
		transform.parent.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
	}

	public void PlayGilWalkSound()
	{
		soundmanager.PlayGilWalkSound ();
	}
	public void StopGilWalkSound()
	{
		soundmanager.StopGilWalkSound ();
	}

	public void PlayGilReadySound()
	{
		soundmanager.PlayGilReadySound ();
	}

	public void PlayGilSetSound()
	{
		soundmanager.PlayGilSetSound ();
	}
}
