﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StartButtonScript : MonoBehaviour {

	public GameObject[] FirstHiddenObjects;

	public GameObject[] FirstShownObjects;

	public GameObject howToImage;

	public GameObject[] leftRightInputBtns;

	//public GameObject hideBgPanel;

	//[SerializeField]
	//private Image imgHowTo;

	public static bool gamePlayed = false;

	//public static float bannerTransition = 500;

	private bool howToShown = false;

	private Soundmanager Snd_mngr
	{
		get{
			if(_snd_mngr == null){
				_snd_mngr = GameObject.Find("Audiomanager")
					.GetComponent<Soundmanager>();
			}
			return _snd_mngr;
		}
	}
	Soundmanager _snd_mngr;

	/*
	private BannerCtrl bannerCtrl{
		get{
			if(_bannerCtrl == null){
				_bannerCtrl = GameObject.FindWithTag("banner")
					.GetComponent<BannerCtrl>();
			}
			return _bannerCtrl;
		}
	}private BannerCtrl _bannerCtrl;
	*/

	// Use this for initialization
	void Start () {
		if(gamePlayed){
			startBtnClicked();
		}

		Application.targetFrameRate = 60; // ターゲットフレームレートを60に設定
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyUp(KeyCode.Escape))
		{
			if(gamePlayed){

			}else{
				if(howToShown){
					ShowHideHowTo();
				}else{
					Application.Quit();
				}

			}
			
		}
	}

	public void startBtnClicked(){
		foreach(GameObject go in FirstHiddenObjects){
			go.SetActive(true);
		}

		if (BannerCtrl.isGameStartedOnce) {
			Snd_mngr.PlayRetryMusic();
		} else {
			Snd_mngr.PlayMusic ();
		}

		foreach(GameObject go in FirstShownObjects){

			//if(go.tag == "banner"){
			//	go.GetComponent<RectTransform>().anchoredPosition = 
			//		bannerTransition * Vector2.up;
			//}else
			{
				go.SetActive(false);
			}

		}

		/*if (bannerCtrl != null) {
			bannerCtrl.HideBanner ();
		}*/
		if(BannerCtrl.component != null)
			BannerCtrl.component.HideBanner ();

		if(PlayerPrefs.GetInt("Score") == 0){
			howToImage.SetActive(true);

			/*foreach(GameObject go in leftRightInputBtns){
				go.SetActive(false);
			}*/
			//hideBgPanel.SetActive(true);
		}

		//gameObject.SetActive (false);

	}

	public void ShowHideHowTo()
	{
		if (!gamePlayed) {
			howToShown = !howToShown;
			
			foreach (Transform children in transform) {
				children.gameObject
					.SetActive (!children.gameObject.activeSelf);
			}
		} else {
			howToImage.SetActive(false);

			/*foreach(GameObject go in leftRightInputBtns){
				go.SetActive(true);
			}*/
			//hideBgPanel.SetActive(false);
		}

	}

}
