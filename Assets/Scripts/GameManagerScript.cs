﻿using UnityEngine;
using System.Collections;

public class GameManagerScript : MonoBehaviour {

	public Soundmanager soundManager;

	public bool deletePrefs = false;

	public static bool potionFlag = false;

	// Use this for initialization
	void Start () {
		if(deletePrefs){
			PlayerPrefs.DeleteAll();
		}

		if(BannerCtrl.isGameStartedOnce == false){
			soundManager.PlayTitleSound();
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
