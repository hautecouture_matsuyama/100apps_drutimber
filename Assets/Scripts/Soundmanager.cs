﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Soundmanager : MonoBehaviour {

	public AudioSource[] Audios;
	//public UISprite SoundSprite;

	//public string SoundOnName = "btn-sOn";
	//public string SoundOffName = "btn-sOff";

	[SerializeField]
	private Sprite sprSndOn;
	[SerializeField]
	private Sprite sprSndOff;
	[SerializeField]
	private Image sprImg;

	[SerializeField]
	private float echoInterval;
	[SerializeField]
	private float echoDecrease;
	[SerializeField]
	private float echoMinimumVol;

	//bool isGameOver = false;

	enum Sounds {Hit, GameOver, Main, Move, Normal, Title, Muteki, GilWalk, GilReady, GilSet};

	// Use this for initialization
	void Awake () {
		if(PlayerPrefs.HasKey("sound")){
			if(PlayerPrefs.GetInt("sound") == 0)
			{
				//SoundSprite.spriteName = SoundOnName;
				sprImg.sprite = sprSndOn;
			}else{
				//SoundSprite.spriteName = SoundOffName;
				sprImg.sprite = sprSndOff;
			}
		}
		else PlayerPrefs.SetInt("sound",0);
	
		//PlayMusic();
	}

	public void PlayHitSound()
	{
		if(PlayerPrefs.GetInt("sound")	== 0)
			Audios[(int)Sounds.Hit].Play();
	}

	public void PlayOneShotHitSound()
	{
		if (PlayerPrefs.GetInt ("sound") == 0)
			Audios [(int)Sounds.Hit].PlayOneShot (Audios [(int)Sounds.Hit].clip, 1);
	}

	public void PlayGameoverSound()
	{

		if(PlayerPrefs.GetInt("sound")	== 0)
			Audios[(int)Sounds.GameOver].Play();


		//StartCoroutine (PlayGOSoundCrtn());
	}
	IEnumerator PlayGOSoundCrtn()
	{
		float volume = 1;

		while(volume > echoMinimumVol){
			if (PlayerPrefs.GetInt ("sound") == 0)
				Audios [(int)Sounds.GameOver].PlayOneShot (Audios [(int)Sounds.GameOver].clip, volume);

			volume *= echoDecrease;

			yield return new WaitForSeconds(echoInterval);
		}
	}

	public void PlayMusic()
	{
		//if (PlayerPrefs.GetInt ("sound") == 0) 
		{
			//Audios [2].Play ();
			StartCoroutine(PlayMusicCrtn());
		}
	}
	IEnumerator PlayMusicCrtn()
	{
		while(Audios[(int)Sounds.Title].isPlaying == true){
			yield return null;
		}

		if (PlayerController.gameoverCheck == false && GameManagerScript.potionFlag == false) {
			if (PlayerPrefs.GetInt ("sound") == 0)
				Audios [(int)Sounds.Main].Play ();
		}
	}

	public void PlayMoveSound(float waitTime)
	{
		/*if (PlayerPrefs.GetInt ("sound") == 0) {
			if(Audios [3].enabled == true){
				Audios [3].PlayOneShot (Audios [3].clip, 1);
			}
		}
		*/

		StartCoroutine (PlayMoveSoundCrtn(waitTime));
	}
	IEnumerator PlayMoveSoundCrtn(float waitTime){

		yield return new WaitForSeconds (waitTime);

		if (PlayerPrefs.GetInt ("sound") == 0) {
			if(Audios [(int)Sounds.Move].enabled == true){
				Audios [(int)Sounds.Move].Play();
			}
		}

	}

	public void PlayNormalSound()
	{
		if (PlayerPrefs.GetInt ("sound") == 0)
			Audios [(int)Sounds.Normal].Play();
	}

	public void PlayTitleSound()
	{
		if (PlayerPrefs.GetInt ("sound") == 0)
			Audios [(int)Sounds.Title].Play ();
	}

	public void PlayMutekiSound()
	{
		if (PlayerPrefs.GetInt ("sound") == 0)
			Audios [(int)Sounds.Muteki].Play ();
	}

	public void PlayRetryMusic()
	{
		PlayTitleSound ();

		Invoke ("PlayMusic", 1f);	//イントロに１秒はかかると想定
	}

	public void StopMusic()
	{
		Audios[(int)Sounds.Main].Stop();
	}
	public void PauseMusic()
	{
		Audios [(int)Sounds.Main].Pause ();
	}

	public void StopMoveSound()
	{
		Audios[(int)Sounds.Move].Stop();
	}

	public void StopTitleSound()
	{
		//isGameOver = true;

		Audios [(int)Sounds.Title].Stop ();

	}

	public void StopMutekiSound()
	{
		Audios [(int)Sounds.Muteki].Stop ();
		
	}

	public void changeSndOnOff()
	{
		if(PlayerPrefs.GetInt("sound")	== 0)
		{
			PlayerPrefs.SetInt("sound",1);
			sprImg.sprite = sprSndOff;

			Audios [(int)Sounds.Title].Stop ();
		}else
		{
			PlayerPrefs.SetInt("sound",0);
			sprImg.sprite = sprSndOn;

			//if(PlayerController.gameoverCheck == false)
			//	Audios [(int)Sounds.Title].Play ();
		}
	}

	public void PlayGilWalkSound()
	{
		if (PlayerPrefs.GetInt ("sound") == 0// && Audios[(int)Sounds.GilWalk].isPlaying == false
		    ) {
			//Audios[(int)Sounds.GilWalk].PlayOneShot(Audios[(int)Sounds.GilWalk].clip, 1);
			Audios[(int)Sounds.GilWalk].Play();
		}
	}
	public void StopGilWalkSound()
	{
		Audios [(int)Sounds.GilWalk].Stop ();
	}

	public void PlayGilReadySound()
	{
		if (PlayerPrefs.GetInt ("sound") == 0) {
			Audios[(int)Sounds.GilReady].Play();
		}
	}
	public void PlayGilSetSound()
	{
		if (PlayerPrefs.GetInt ("sound") == 0) {
			Audios[(int)Sounds.GilSet].Play();
		}
	}
}
