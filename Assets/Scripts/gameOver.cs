﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class gameOver : MonoBehaviour {

	//public UILabel[] ScoreLabels;

	//public GameObject gameOverCanvas;
	public Text[] BestScoreText;
	public Text[] ScoreText;

	public Animator shareAnimCtrl;

	public string[] shareAnimStateString;

	public string[] shareAnimTrigger; 

	private int[] shareAnimStateHash;

	[System.NonSerialized]
	public bool isScoreUpdated = false;

	// Use this for initialization
	void OnEnable () {

		//GameObject.Find("Audiomanager").GetComponent<Soundmanager>().StopMusic();

		//GameObject.Find("Audiomanager").GetComponent<Soundmanager>().PlayGameoverSound();

		for(int i=1; i<BestScoreText.Length; i++)
		{
			int oldScore = PlayerPrefs.GetInt("Score");

			if(oldScore < PlayerController.Score){
				//ハイスコア更新
				isScoreUpdated = true;
			}

			if(oldScore > PlayerController.Score)
				BestScoreText[i].text ="" + oldScore;
			else
			{
				BestScoreText[i].text = "" + PlayerController.Score;
				PlayerPrefs.SetInt("Score",PlayerController.Score);
			}
		}

		for(int i=1; i<ScoreText.Length; i++)
		{
			ScoreText[i].text = "" + PlayerController.Score;
		}

		shareAnimStateHash = new int[shareAnimStateString.Length];
		for(int i=0; i<shareAnimStateString.Length; i++){
			shareAnimStateHash[i] = 
				Animator.StringToHash(shareAnimStateString[i]);
		}
	}

	void Update()
	{

	}

	public void ShareButtonClicked()
	{
		AnimatorStateInfo anim = 
			shareAnimCtrl.GetCurrentAnimatorStateInfo(0);

		if(anim.nameHash == shareAnimStateHash[0] || 
		   anim.nameHash == shareAnimStateHash[3]){
			shareAnimCtrl.SetTrigger(shareAnimTrigger[0]);
		}else if(anim.nameHash == shareAnimStateHash[1] || 
		         anim.nameHash == shareAnimStateHash[2]){
			shareAnimCtrl.SetTrigger(shareAnimTrigger[1]);
		}
	}

	public bool isShareIconsOpend()
	{
		AnimatorStateInfo anim = 
			shareAnimCtrl.GetCurrentAnimatorStateInfo(0);

		if (anim.nameHash == shareAnimStateHash [1] || 
			anim.nameHash == shareAnimStateHash [2]) {
			return true;
		} else {
			return false;
		}
	}
}
