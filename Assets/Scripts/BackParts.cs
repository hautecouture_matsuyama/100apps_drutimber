﻿using UnityEngine;
using System.Collections;

public class BackParts : MonoBehaviour {

	public GameObject[] potions;

	void OnEnable()
	{

	}

	// Use this for initialization
	void Start () {

	}

	public void EnablePotions()
	{
		foreach(GameObject potion in potions){
			potion.SetActive(true);
			potion.GetComponent<UIPosCtrl>()
				.UI_Element.gameObject.SetActive(true);
		}
	}

	// Update is called once per frame
	void Update () {
	
	}
}
