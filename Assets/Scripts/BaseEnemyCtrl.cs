﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BaseEnemyCtrl : MonoBehaviour {

	[SerializeField]
	protected string[] animTriggers = {"TurnLeft", "TurnRight", "Move"};
	[SerializeField]
	protected string[] idleAnimStates = {"Base Layer.TurnLeft", "Base Layer.TurnRight"};
	[SerializeField]
	protected float minWaitTime = 2;
	[SerializeField]
	protected float maxWaitTime = 4;
	[SerializeField]
	protected float attackTimeLength = 1;

	bool isAttacking = false;

	bool isRendered = false;

	EnemyCommon enemyCommon{
		get{
			if(_enemyCommon == null){
				_enemyCommon = gameObject.GetComponent<EnemyCommon>();
			}
			return _enemyCommon;
		}
	}EnemyCommon _enemyCommon;

	Animator animCtrl{
		get{
			if(_animCtrl == null){
				_animCtrl = gameObject.GetComponent<Animator>();
			}
			return _animCtrl;
		}
	}
	Animator _animCtrl;

	protected AudioSource soundSE{
		get{
			if(_soundSE == null){
				_soundSE = gameObject.GetComponent<AudioSource>();
			}
			return _soundSE;
		}
	}AudioSource _soundSE;

	// Use this for initialization
	protected void Start () {
		StartCoroutine (AnimCoroutine());
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator AnimCoroutine()
	{
		if(Random.value < 0.5f){
			animCtrl.SetTrigger("TurnRight");
		}

		while(true){

			if(isAttacking){
				yield return null;
			}else{
				yield return 
					new WaitForSeconds(Random.Range(minWaitTime, maxWaitTime));
				
				int triggerIndex = Random.Range (0, animTriggers.Length);

				string animTrigger = animTriggers[triggerIndex];

				AnimatorStateInfo stateInfo = animCtrl.GetCurrentAnimatorStateInfo(0);

				float actionLength = 0f;

				//同じ動きをするのはやめて攻撃する
				foreach(string state in idleAnimStates){
					if(Animator.StringToHash(state) == stateInfo.nameHash){

						if(state == idleAnimStates[0]){
							if(Random.value < 0.5f){
								animTrigger = animTriggers[1];
							}else{
								animTrigger = animTriggers[2];
								actionLength = attackTimeLength;
							}
						}

						if(state == idleAnimStates[1]){
							if(Random.value < 0.5f){
								animTrigger = animTriggers[2];
								actionLength = attackTimeLength;
							}else{
								animTrigger = animTriggers[0];
							}
						}

						break;
					}
				}

				//表示されているときのみアニメーション
				if(isRendered)
					animCtrl.SetTrigger (animTrigger);

				yield return new WaitForSeconds(actionLength);

			}

			if(PlayerController.gameoverCheck)
				break;
		}

	}

	public void SetAttacking(){
		isAttacking = true;
	}

	public void ResetAttackingLeft(){
		isAttacking = false;

		enemyCommon.direction = -1;
	}

	public void ResetAttackingRight(){
		isAttacking = false;
		
		enemyCommon.direction = 1;
	} 

	public void PlaySoundSE()
	{
		if (isRendered) {
			if (PlayerPrefs.GetInt ("sound") == 0)
				soundSE.Play ();
		}
	}

	void OnWillRenderObject()
		
	{
		
		#if UNITY_EDITOR
		
		if(Camera.current.name != "SceneCamera"  && Camera.current.name != "Preview Camera")
			
			#endif
			
		{
			
			isRendered = true;
			
		}
		
	}
	

}
