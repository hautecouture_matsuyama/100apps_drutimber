﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class MoveBackground : MonoBehaviour {

	//サッカーボールと同じサイズにするのがよいと思う
	public float speed = 2f;
	//public float horizontalDirection = -1; //move from right to left

	public static bool Slide1point = false;

	private PlayerController player
	{
		get{
			if(_player == null){
				_player = GameObject.Find("Player")
					.GetComponent<PlayerController>();
			}

			return _player;
		}
	}
	private PlayerController _player;
	
	// Use this for initialization
	void Start () {
		Slide1point = false;

	}

	// Update is called once per frame
	void Update () {
		/*
		if(Slide1point)
		{
			PlayerController.OneTimeOnly = true;
			PlayerController.Score ++ ;
			Slide1point = false;
			//move the root BG from right to left
			transform.Translate(new Vector3(0, speed * horizontalDirection * Time.deltaTime, 0));
		}
		*/
	}

	//FixedUpdateでやらないと毎回落ちる幅が変わる！！！
	//というより時間を使わない方がよいのでは？
	void FixedUpdate()
	{
		if(Slide1point)
		{
			PlayerController.OneTimeOnly = true;
			PlayerController.Score ++ ;
			Slide1point = false;
			//move the root BG from right to left
			//transform.Translate(new Vector3(0, -speed, 0));

			transform.DOMove(transform.position
			                 + new Vector3(0, -speed, 0), player.minTweenTime);

			//Debug.Log("Time.fixedDeltaTime: " + Time.fixedDeltaTime);
		}
	}
	
}