﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using NendUnityPlugin.AD;
using DG.Tweening;
using System.Linq;
using DG.Tweening;

public class PlayerController : MonoBehaviour {

	public static int Score;
    //public Canvas ShareCanvas;

	public GameObject[] GameOverWindow;
	public GameObject[] LeftrightButtons;
	public GameObject[] tapTap;

	//public UILabel Scorelabel;
	//public UILabel Levellabel;

	public Text[] ScoreTexts;
	public Text[] LevelTexts;
	public Text[] LevelString;

	public Sprite[] AnimationSprites;
	public int[] AnimSpriteFrameCount;
	public float[] AnimPosX;
	public Sprite[] AnimationSprites_right;

	public Sprite[] ExplosionSprite;
	public Sprite[] NextExplosionSprite;
	public Ease downEaseType;

	private float numImages = 2;
	private bool hitleft = false;
	private bool hitRight = false;

	//public Sprite[] hamSprite;
	//public Sprite[] hamDamSprite;

	//public GameObject Leaf;
	public GameObject Deastoyparticle;
	public int DeastoyparticleOrder;
	public float effectDistance = 0;

	private int motion;

	private bool runOnce;
	public static bool OneTimeOnly;

	public GameObject FallingLeaves;
	//public GameObject HitParticle;

	//public GameObject HitEffect;
	//public int HitEffectOrder;

	//public GameObject OpenShares;
	//public GameObject[] ShareButtons;

	//public GameObject HamsTower;

	//public string shareBtnSpriteName;
	//public string shareCloseBtnSpriteName;

	public GameObject scorePanel;
	//public GameObject BannerGO;
	public GameObject MusicGO;

	public HealthBar nekoHP;
	public Animator HPAnimCtrl;

	public GameObject timeOverObject;

	public bool debugFlag = false;

	public float minTweenTime = 0.1f;

	[SerializeField]
	private float Min_minTweenTime = 0.02f;

	[SerializeField]
	private float limitHeightDist;

	//[SerializeField]
	//private GameObject[] enemies;

	[SerializeField]
	private int accelDownScore;

	[SerializeField]
	private GameObject explanation;

	[SerializeField]
	private string[] enemyTagList;

	private float org_minTweenTime;

	float lastTouchTime;

	//private Queue SoccerBalls;
	public TowerController towerCtrl;

	public SpriteRenderer playerSprn;

	public UISprite tapLeftSprite, tapRightSprite;

	public float intervalBeforeAd = 1.0f;

	public ResizeBackground backgroundManager;

	private bool posLeft = true;

	private Soundmanager smanager
	{
		get{
			if(_smanager == null){
				_smanager = 
					GameObject.Find("Audiomanager").GetComponent<Soundmanager>();
			}

			return _smanager;
		}
	}
	private Soundmanager _smanager;

	[SerializeField]
	private Animator animCtrl;

	public Animator protectorAnim;

	public TrailRenderer protectorTrail;

	public float potionTime = 7f;

	public GameObject[] potionHukidashi;

	Queue potionCrtns;

	/*private Animator animCtrl
	{
		get{
			if(_animCtrl == null){
				_animCtrl = gameObject.GetComponentInChildren<Animator>();
			}
			return _animCtrl;
		}

	}
	private Animator _animCtrl;*/

	/*
	private BannerCtrl bannerCtrl{
		get{
			if(_bannerCtrl == null){
				_bannerCtrl = GameObject.FindWithTag("banner")
					.GetComponent<BannerCtrl>();
			}
			return _bannerCtrl;
		}
	}private BannerCtrl _bannerCtrl;
	*/

	//private Vector2 bannerInitPos;

	// Use this for initialization
	void Start () {

		//Levellabel.enabled = false;

		foreach(Text txt in LevelTexts){
			txt.enabled = false;
		}
		foreach(Text txt in LevelString){
			txt.enabled = false;
		}

		OneTimeOnly = true;
		runOnce = true;
		if(gameoverCheck)
		{
			Time.timeScale = 1;

			gameoverCheck = false;

			foreach(GameObject lf in LeftrightButtons)
				lf.SetActive(true);

			foreach(GameObject chd in GameOverWindow)
				chd.SetActive(false);

			foreach(GameObject lf in tapTap)
				lf.SetActive(true);

		}else{

			Time.timeScale = 0;

			foreach(GameObject lf in LeftrightButtons)
				lf.SetActive(false);
			foreach(GameObject lf in tapTap)
				lf.SetActive(false);
		}

		//InitHamsters ();

		if(!StartButtonScript.gamePlayed)
		{
			OnPlay();
		}

		if(debugFlag){
			//StartCoroutine(AutoPlayCoroutine());
			Time.timeScale = 0.05f;

		}

		lastTouchTime = Time.time;

		/*SpriteRenderer[] tmpSprn = transform.
			GetComponentsInChildren<SpriteRenderer>();
		foreach(SpriteRenderer sprn in tmpSprn){
			if(sprn != null){
				if(sprn.transform.parent == transform){
					playerSprn = sprn;
				}
			}
		}*/

		org_minTweenTime = minTweenTime;

		potionCrtns = new Queue ();

		//bannerInitPos = BannerGO.GetComponent<RectTransform>().anchoredPosition;
	}

	/*public void InitHamsters()
	{
		SortedList<float, GameObject> SortedSoccerBalls = 
			new SortedList<float, GameObject> ();

		HamsTower.SetActive (true);

		GameObject[] balls = GameObject.FindGameObjectsWithTag ("SoccerBall");
		
		foreach(GameObject go in balls){
			SortedSoccerBalls.Add(go.transform.position.y, go);
		}
		
		SoccerBalls = new Queue ();
		
		for(int i=0; i<SortedSoccerBalls.Count; i++){
			float key = SortedSoccerBalls.Keys[i];

			SpriteRenderer sprn = 
				SortedSoccerBalls[key].GetComponent<SpriteRenderer>();
			sprn.sprite = hamSprite[Random.Range(0, hamSprite.Length)];
			
			SoccerBalls.Enqueue(
				SortedSoccerBalls[key]);
		}
	}*/
	
	// Update is called once per frame
	void Update () {
		//Scorelabel.text = ""+Score;
		foreach(Text txt in ScoreTexts){
			txt.text = ""+Score;
		}

		//プレーヤーが反対側に移っても向きが変わらないことがあったので修正
		/*if(hitleft || hitRight){
			if (posLeft) {
				hitleft = true;
				hitRight = false;
			} else {
				hitleft = false;
				hitRight = true;
			}
		}*/

		if (posLeft && hitRight) {
			hitleft = true;
			hitRight = false;
		}
		if (!posLeft && hitleft) {
			hitleft = false;
			hitRight = true;
		}

		if(hitleft)
		{
			motion++;

			for(int i=0; i<AnimSpriteFrameCount.Length; i++)
			{
				int motion_min = (i == 0)? 0: AnimSpriteFrameCount[i-1];

				if(motion >= motion_min && motion < AnimSpriteFrameCount[i])
				{
					//this.transform.GetComponent<SpriteRenderer>().sprite = AnimationSprites[i];
					playerSprn.sprite = AnimationSprites[i];
					playerSprn.transform.localPosition = 
						new Vector3(AnimPosX[i], 
						            playerSprn.transform.localPosition.y, 
						            playerSprn.transform.localPosition.z);
					break;
				}
			}

			if(motion >= AnimSpriteFrameCount[AnimSpriteFrameCount.Length - 1])
			{
				//this.transform.GetComponent<SpriteRenderer>().sprite = AnimationSprites[0];
				playerSprn.sprite = AnimationSprites[0];
				playerSprn.transform.localPosition = 
					new Vector3(AnimPosX[0], 
					            playerSprn.transform.localPosition.y, 
					            playerSprn.transform.localPosition.z);

				motion = 0;
				hitleft = false;

			}

			if(gameoverCheck){
				//this.transform.GetComponent<SpriteRenderer>().sprite = ExplosionSprite[0];
				playerSprn.sprite = ExplosionSprite[0];
			}
		}else if(hitRight)
		{
			motion++;

			for(int i=0; i<AnimSpriteFrameCount.Length; i++)
			{
				int motion_min = (i == 0)? 0: AnimSpriteFrameCount[i-1];
				
				if(motion >= motion_min && motion < AnimSpriteFrameCount[i])
				{
					//this.transform.GetComponent<SpriteRenderer>().sprite = AnimationSprites[i];
					playerSprn.sprite = AnimationSprites_right[i];
					playerSprn.transform.localPosition = 
						new Vector3(-AnimPosX[i], 
						            playerSprn.transform.localPosition.y, 
						            playerSprn.transform.localPosition.z);
					break;
				}
			}
			
			if(motion >= AnimSpriteFrameCount[AnimSpriteFrameCount.Length - 1])
			{
				//this.transform.GetComponent<SpriteRenderer>().sprite = AnimationSprites[0];
				playerSprn.sprite = AnimationSprites_right[0];
				playerSprn.transform.localPosition = 
					new Vector3(-AnimPosX[0], 
					            playerSprn.transform.localPosition.y, 
					            playerSprn.transform.localPosition.z);
				motion = 0;
				hitRight = false;
				
			}

			if(gameoverCheck){
				//this.transform.GetComponent<SpriteRenderer>().sprite = ExplosionSprite[0];
				playerSprn.sprite = ExplosionSprite[1];
			}
		}


		if (Input.GetKeyUp(KeyCode.Escape))
		{
			if(StartButtonScript.gamePlayed){

				if(!GameOverWindow[0].activeSelf)
				{
					StartButtonScript.gamePlayed = false;
					Application.LoadLevel(Application.loadedLevel);

					/*if(bannerCtrl != null){
						bannerCtrl.SetBanner();
					}*/

					if(BannerCtrl.component != null)
						BannerCtrl.component.SetBanner();
				}
				else
				{
					gameOver go = GameOverWindow[0].GetComponent<gameOver>();

					if(go.isShareIconsOpend()){
						go.ShareButtonClicked();
					}else{
						StartButtonScript.gamePlayed = false;
						Application.LoadLevel(Application.loadedLevel);

						/*if(bannerCtrl != null){
							bannerCtrl.SetBanner();
						}*/

						if(BannerCtrl.component != null)
							BannerCtrl.component.SetBanner();
					}

				}

			}else{

			}

		}
	}

	IEnumerator AutoPlayCoroutine()
	{

		//float waitTime = 0.2f;

		float distanceForChanging = 3.1357f;

		int waitCount = 10;

		bool left = true;

		while(true){

			//yield return new WaitForSeconds(waitTime);

			for(int i=0; i<waitCount / Time.timeScale; ++i){
				yield return null;
			}

			GameObject[] enemyObjects = 
				GameObject.FindGameObjectsWithTag("Enemy");

			if(transform.position.x < 0){

				foreach(GameObject go in enemyObjects){
					
					float y_dist = Mathf.Abs(
						go.transform.position.y - transform.position.y);

					int sign = (int)Mathf.Sign(go.transform.position.x);

					if(y_dist < distanceForChanging && sign == -1 && left)
					{
						Moveright();	//1
						left = false;
						break;
					}
					
				}
				
				if(left){
					Moveleft();	//2

				}
			}else{

				foreach(GameObject go in enemyObjects){

					float y_dist = Mathf.Abs(
						go.transform.position.y - transform.position.y);

					int sign = (int)Mathf.Sign(go.transform.position.x);

					if(y_dist < distanceForChanging && sign == 1 && !left)
					{
						Moveleft();	//3
						left = true;
						break;
					}

				}

				if(!left){
					Moveright();	//4

				}
			}

		}

	}

	void Moveleft()
	{
		if(explanation.activeInHierarchy){
			return;
		}

		if(runOnce)
		{
			runOnce = false;
			//healthBar.enabled = true;
			nekoHP.enabled = true;

			//foreach(GameObject lf in tapTap)
			//	lf.SetActive(false);


		}

		if(Time.time - lastTouchTime < minTweenTime){
			return;
		}else{
			lastTouchTime = Time.time;
		}

		playerSprn.sprite = AnimationSprites[0];

		tapLeftSprite.spriteName = "btn_tap_left_01";
		tapRightSprite.spriteName = "btn_tap_right_02";

		if (!posLeft/*transform.localPosition.x > 0*/) 
		{
			posLeft = true;

			//transform.localScale = new Vector3(-transform.localScale.x, 
			//                                   transform.localScale.y, 
			//                                   transform.localScale.z);

			transform.DOMove(new Vector3 (-transform.localPosition.x, transform.localPosition.y, transform.localPosition.z), minTweenTime);

			/*
			bool soundOn = true;


			foreach(GameObject go in enemies){
				if(go.activeInHierarchy){
					if(Mathf.Abs(go.transform.position.y - transform.position.y) < limitHeightDist){
						soundOn = false;
						break;
					}
				}

			}


			if(soundOn)
				smanager.PlayMoveSound(0);
			*/	

		}
		else
		{
			smanager.PlayOneShotHitSound();

			//UpdateHealthbar();

			//healthBar.IncreaseHealthBar();
			nekoHP.IncreaseHealthBar();

			StartCoroutine(
				towerCtrl.KickSoccerBall(1, AnimSpriteFrameCount[0]));

			MoveBackground.Slide1point = true;

			hitleft = true;
			motion = 0;
		//	this.transform.GetComponent<SpriteRenderer>().sprite = AnimationSprites[0];

			Vector3 forward = transform.TransformDirection(Vector3.right) * 10;
			Debug.DrawRay(transform.position, forward, Color.white);
			
		}

	}

	IEnumerator FadeOutSprite(SpriteRenderer sprn)
	{
		float fadeSpeed = 2;
		float explodeSpeed = 3;
		float scaleMax = 1.5f;

		while(sprn.color.a > 0){
			if(sprn.color.a - fadeSpeed*Time.deltaTime < 0){
				break;
			}else{
				sprn.color -= new Color(0, 0, 0, fadeSpeed*Time.deltaTime);

				if(sprn.transform.localScale.x < scaleMax){
					sprn.transform.localScale += 
						new Vector3(explodeSpeed*Time.deltaTime, 
						            explodeSpeed*Time.deltaTime, 0);
				}

				yield return null;
			}

		}

		Destroy (sprn.gameObject);
	}

	/*void UpdateHealthbar()
	{
		//HealthBar helthBar = GameObject.Find("Progress Bar").GetComponent<HealthBar>();
		
		//helthBar.healthslider.value += Time.deltaTime*3f;
		healthBar.health += 0.3f;
	}*/

	void Moveright()
	{
		if(explanation.activeInHierarchy){
			return;
		}

		if(runOnce)
		{
			runOnce = false;
			//healthBar.enabled = true;
			nekoHP.enabled = true;

			//foreach(GameObject lf in tapTap)
			//	lf.SetActive(false);
		}

		if(Time.time - lastTouchTime < minTweenTime){
			return;
		}else{
			lastTouchTime = Time.time;
		}

		playerSprn.sprite = AnimationSprites_right[0];

		tapLeftSprite.spriteName = "btn_tap_left_02";
		tapRightSprite.spriteName = "btn_tap_right_01";

		if (posLeft/*transform.localPosition.x < 0*/) 
		{
			posLeft = false;

			//transform.localScale = new Vector3(-transform.localScale.x, 
			//                                   transform.localScale.y, 
			//                                   transform.localScale.z);

			transform.DOMove(new Vector3 (-transform.localPosition.x, transform.localPosition.y, transform.localPosition.z), minTweenTime);

			/*
			bool soundOn = true;


			foreach(GameObject go in enemies){
				if(go.activeInHierarchy){
					if(Mathf.Abs(go.transform.position.y - transform.position.y) < limitHeightDist){
						soundOn = false;
						break;
					}
				}
				
			}


			if(soundOn)
				smanager.PlayMoveSound(0);

			*/
		}
		else
		{
			smanager.PlayOneShotHitSound();
			hitRight = true;
			motion = 0;

			//healthBar.IncreaseHealthBar();
			nekoHP.IncreaseHealthBar();

			StartCoroutine(
				towerCtrl.KickSoccerBall(-1, AnimSpriteFrameCount[0]));

			MoveBackground.Slide1point = true;
		}
	}

	public void SetMinTweenTime(int preLevel, int maxLevel)
	{
		minTweenTime = org_minTweenTime
			- (org_minTweenTime - Min_minTweenTime) * preLevel / maxLevel;
	}
	/*
	//dir -- ボールを蹴る方向（1,-1）
	IEnumerator KickSoccerBall(int dir)
	{
		int counter = 0;

		int waitCount = AnimSpriteFrameCount[0]
			//(AnimSpriteFrameCount[0] + AnimSpriteFrameCount[1]) / 2
				;

		while(counter < waitCount){
			counter++;
			yield return null;
		}
		
		Vector3 ballPosition = new Vector3(0
		                                   , this.transform.localPosition.y - .4f
		                                   ,this.transform.localPosition.z);
		
		GameObject leafthrow = Instantiate(Leaf, ballPosition + dir * 0.5f * Vector3.right,
		                                   Quaternion.identity//Quaternion.Euler(0, 0, 85)
		                                   ) as GameObject;

		//ダメージスプライトのセット
		GameObject hamGo = (GameObject)SoccerBalls.Peek ();
		SpriteRenderer hamSprn = hamGo.GetComponent<SpriteRenderer>();
		for(int i=0; i<hamSprite.Length; i++){
			if(hamSprite[i] == hamSprn.sprite){
				SpriteRenderer hamDamSprn = leafthrow.GetComponent<SpriteRenderer>();
				hamDamSprn.sprite = hamDamSprite[i];
			}
		}

		leafthrow.rigidbody2D.AddForce(new Vector3(dir*300,Random.Range(0, 300),0));

		leafthrow.transform.localScale = 
			new Vector3 (dir * leafthrow.transform.localScale.x, 
			             leafthrow.transform.localScale.y, 
			             leafthrow.transform.localScale.z);

		leafthrow.rigidbody2D.AddTorque(Random.Range (-300, 300));
		
		GameObject hitEffectObject = Instantiate(
			HitEffect, ballPosition, 
			Quaternion.Euler(0, 0, 360*Random.value)) as GameObject;

		foreach(ParticleSystem ps in hitEffectObject
		        .GetComponentsInChildren<ParticleSystem>())
		{
			ps.renderer.sortingOrder = HitEffectOrder;

		}

		//StartCoroutine(
		//	FadeOutSprite(hitEffectObject.GetComponent<SpriteRenderer>()));
		
		ResetSoccerBalls();

	}*/

	/*void ResetSoccerBalls(){
		int ballCount = SoccerBalls.Count;
		float hightFixedCnst = 1f;

		GameObject ball = (GameObject)SoccerBalls.Dequeue ();

		ball.GetComponent<SpriteRenderer>().sprite = 
			hamSprite[Random.Range(0, hamSprite.Length)];

		float highestHeight = ball.transform.position.y;
		foreach(Object obj in SoccerBalls){
			GameObject go = (GameObject)obj;
			if(highestHeight < go.transform.position.y){
				highestHeight = go.transform.position.y;
			}
		}

		ball.transform.position = 
			new Vector3 (0, highestHeight + hightFixedCnst, 0);

		SoccerBalls.Enqueue (ball);
	}*/

	public static bool gameoverCheck = false;

	void OnTriggerEnter2D(Collider2D c)
	{

		if(//c.tag == "Enemy"
		   enemyTagList.Contains(c.tag) && GameManagerScript.potionFlag == false)
		{
			//FallingLeaves.SetActive(true);	サッカーなので葉っぱが降るのはおかしい

			Vector3 EffectPosition = //new Vector3(transform.localPosition.x,
			                       //            transform.localPosition.y+1,
			                       //            transform.localPosition.z)
				effectDistance*c.transform.position
				 + (1-effectDistance)*transform.position;

			GameObject particle = Instantiate(Deastoyparticle, 
			                                  EffectPosition,
			            Quaternion.Euler(90,0,0)) as GameObject;

			ParticleSystem[] particleSystems = 
				particle.GetComponentsInChildren<ParticleSystem>();

			foreach(ParticleSystem ps in particleSystems){
				ps.GetComponent<Renderer>().sortingOrder = DeastoyparticleOrder;
			}

			GameOver(0);

			DamageMotion();

			/*
			playerSprn.transform.DOShakePosition(0.5f,0.5f);

			float rotateAngle = 30;
			if(transform.position.x < 0){

			}else{
				rotateAngle = -rotateAngle;
			}
			playerSprn.transform.DOLocalRotate(new Vector3(0,0,rotateAngle),0.1f);
			*/
		}
		else if(c.tag == "Potion")
		{
			if(potionCrtns.Count > 0)
			{
				StopCoroutine((Coroutine)potionCrtns.Dequeue());
			}

			//ebug.Log("Potion Got!");
			c.gameObject.GetComponent<UIPosCtrl>()
				.UI_Element.gameObject.SetActive(false);

			c.gameObject.SetActive(false);

			if(GameManagerScript.potionFlag == false){
				protectorAnim.SetTrigger("Protect");
				HPAnimCtrl.SetTrigger("Blink");

			}

			GameManagerScript.potionFlag = true;

			protectorTrail.enabled = true;

			smanager.PlayMutekiSound();
			smanager.PauseMusic();
			smanager.StopTitleSound();

			potionCrtns.Enqueue(StartCoroutine(StopMutekiCrtn(potionTime)));
		}

	}

	void DamageMotion()
	{
		float duration = 0.5f;

		playerSprn.transform.DOShakePosition(duration,0.5f);
		Vector3 nockBackVec = -0.3f*Vector3.right;
		
		float rotateAngle = 30;
		if(transform.position.x < 0){
			playerSprn.sprite = ExplosionSprite[0];
		}else{
			playerSprn.sprite = ExplosionSprite[1];
			rotateAngle = -rotateAngle;
			nockBackVec = -nockBackVec;
		}

		float firstDuration = 0.1f;

		playerSprn.transform.DOLocalRotate(new Vector3(0,0,rotateAngle),firstDuration);
		playerSprn.transform.parent.DOMove(
			playerSprn.transform.parent.position + nockBackVec,firstDuration,false);

		//Invoke ("NextDamageMotion", duration);
		StartCoroutine (NextDamageMotion(duration));
	}
	IEnumerator NextDamageMotion(float waitTime)
	{
		yield return new WaitForSeconds (waitTime);

		float rotateAngle = 90;
		if(transform.position.x < 0){
			playerSprn.sprite = NextExplosionSprite[0];
		}else{
			playerSprn.sprite = NextExplosionSprite[1];
			rotateAngle = -rotateAngle;
		}

		float duration = 0.4f;

		playerSprn.transform.DOLocalRotate(new Vector3(0,0,rotateAngle),duration)
			.SetEase(downEaseType);
		playerSprn.transform.parent.DOMove(
			playerSprn.transform.parent.position - 0.2f*Vector3.up, duration, false)
			.SetEase(downEaseType);
	}

	IEnumerator StopMutekiCrtn(float time)
	{
		yield return new WaitForSeconds (time);

		if(GameManagerScript.potionFlag){
			GameManagerScript.potionFlag = false;
			
			protectorAnim.SetTrigger("Reset");
			
			HPAnimCtrl.SetTrigger("Reset");
			
			protectorTrail.enabled = false;
			
			smanager.StopMutekiSound();
			smanager.PlayMusic();
		}

	}





    IEnumerator ShowInterstitial()
    {

        yield return new WaitForSeconds(Random.RandomRange(1.0f, 1.8f));

        if (Random.value >= 0.33f)
        NendAdInterstitial.Instance.Show();

    }

	//0 -- hit
	//1 -- 時間切れ
	public void GameOver(int type)
	{
		//接続されているときの処理
        StartCoroutine(ShowInterstitial());

		if(type == 1){
			float hideTweenTime = 0.4f;
			backgroundManager.GameOverOperation (hideTweenTime);

			//towerCtrl.GameOverOperation(hideTweenTime);
		}


		/*if (transform.position.x < 0) {
			playerSprn.sprite = ExplosionSprite[0];
		} else {
			playerSprn.sprite = ExplosionSprite[1];
		}*/

		gameoverCheck = true;

		smanager.StopMoveSound ();
		smanager.StopMusic();
		smanager.StopTitleSound ();
		smanager.StopMutekiSound ();
		
		smanager.PlayGameoverSound();

		nekoHP.enabled = false;

		scorePanel.SetActive (false);
		/*
		if(bannerCtrl != null){
			bannerCtrl.SetBanner();
		}
		*/
		if (BannerCtrl.component != null) {
			BannerCtrl.component.SetBanner ();
		}

		MusicGO.SetActive (true);

		//if(Leaf != null)
		//{
		//	foreach(GameObject lef in GameObject.FindGameObjectsWithTag("leaf"))
		//	Destroy(lef);
		//}

		timeOverObject.SetActive (true);

		Text timeOverText = timeOverObject.GetComponent<Text>();

		if(type == 0){
			//hit
			timeOverText.text = "";
		}else{
			//時間切れ
			timeOverText.text = "TIME IS UP";
		}

		foreach(GameObject lf in LeftrightButtons)
			lf.SetActive(false);

		foreach(Text txt in ScoreTexts){
			txt.enabled = false;
		}
		foreach(Text txt in LevelTexts){
			txt.enabled = false;
		}
		foreach(Text txt in LevelString){
			txt.enabled = false;
		}

		animCtrl.enabled = true;

		GetComponent<Collider2D>().enabled = false;

		protectorAnim.SetTrigger ("Reset");
		protectorTrail.enabled = false;

		HPAnimCtrl.SetTrigger("Reset");

		foreach(GameObject go in potionHukidashi){
			go.SetActive(false);
		}

		GameManagerScript.potionFlag = false;

		foreach(GameObject lf in tapTap)
			lf.SetActive(false);
	}

	//bool shareOpened = false;
	/*
	void OnShare()
	{

		Vector3[] transitionVec = new Vector3[]{
			new Vector3(0, 57, 0), 
			new Vector3(110, 57, 0), 
			new Vector3(220, 57, 0)
		};

		float transitionTime = .2f;

		for(int i=0; i<ShareButtons.Length; ++i){
			if(!shareOpened){
				ShareButtons[i].SetActive(true);
			}

			Vector3 pos = shareOpened ? Vector3.zero : transitionVec[i];

			iTween.MoveTo(ShareButtons[i], iTween.Hash(
				"position", pos, 
				"islocal", true, 
				"time", transitionTime, 
				"easetype", iTween.EaseType.linear));
		}

		shareOpened = !shareOpened;

		StartCoroutine (setShareOpened(transitionTime, shareOpened));
	}
	IEnumerator setShareOpened(float waitTime, bool opened){
		//動いている途中にボタンを押せないようにする
		UIButton openShareUIButton = OpenShares.GetComponent<UIButton>();
		UIButtonMessage openShareUIButtonMessage = 
			OpenShares.GetComponent<UIButtonMessage> ();

		UIButton[] ShareUIButtons = new UIButton[ShareButtons.Length];
		UIButtonMessage[] ShareUIButtonMessages = 
			new UIButtonMessage[ShareButtons.Length];

		openShareUIButton.enabled = false;
		openShareUIButtonMessage.enabled = false;

		for (int i=0; i<ShareButtons.Length; ++i) {
			ShareUIButtons[i] = 
				ShareButtons[i].GetComponent<UIButton>();
			ShareUIButtonMessages[i] = 
				ShareButtons[i].GetComponent<UIButtonMessage>();

			ShareUIButtons[i].enabled = false;
			ShareUIButtonMessages[i].enabled = false;
		}

		GameObject playBtn = GameObject.Find ("GameOver").transform
			.FindChild("PlayButton").gameObject;
		UIButton PlayUIButton = playBtn.GetComponent<UIButton> ();
		UIButtonMessage PlayUIButtonMessage = 
			playBtn.GetComponent<UIButtonMessage> ();

		PlayUIButton.enabled = false;
		PlayUIButtonMessage.enabled = false;

		//動いている
		yield return new WaitForSeconds (waitTime);

		//動き終わったのでボタンを押せるようにする
		openShareUIButton.enabled = true;
		openShareUIButtonMessage.enabled = true;

		for (int i=0; i<ShareButtons.Length; ++i) {
			ShareUIButtons[i].enabled = true;
			ShareUIButtonMessages[i].enabled = true;
		}

		OpenShares.transform.FindChild("Background").gameObject
			.GetComponent<UISprite>().spriteName = 
				opened ? 
				shareCloseBtnSpriteName : shareBtnSpriteName;

		if(!opened){
			foreach(GameObject go in ShareButtons){
				go.SetActive(false);
			}
		}

		PlayUIButton.enabled = !opened;
		PlayUIButtonMessage.enabled = !opened;
	}
	*/

	public void OnPlay()
	{
        //ShareCanvas.gameObject.SetActive(false);
		Score = 0;
		Time.timeScale = 1;

		//this.gameObject.GetComponent<SpriteRenderer>().enabled = true;

		StartButtonScript.gamePlayed = true;

		BannerCtrl.isGameStartedOnce = true;

		if(gameoverCheck)
		{

			Application.LoadLevel(Application.loadedLevel);

		}else{
			foreach(GameObject chd in GameOverWindow)
				chd.SetActive(false);

			foreach(GameObject lf in LeftrightButtons)
				lf.SetActive(true);

			foreach(GameObject lf in tapTap)
				lf.SetActive(true);

		}

		//StartButtonScript.gamePlayed = true;
	}

	private int a = 1;

	void LateUpdate()
	{
		int oneLevelScores = 20;

		if(Score > accelDownScore){
			oneLevelScores = 50;
		}

		if(Score >= oneLevelScores){
			foreach(GameObject lf in tapTap)
				lf.SetActive(false);
		}

		if(Score % oneLevelScores == 0 && Score > 1 && OneTimeOnly)
		{
			//healthBar.HealthBarSpeedUp();
			nekoHP.HealthBarSpeedUp();
				OneTimeOnly = false;
				a++;
				StartCoroutine("ShowLevel");
		}


	}

	IEnumerator ShowLevel()
	{
		foreach(Text txt in LevelString){
			txt.enabled = true;
		}

		//Levellabel.enabled = true;
		//Levellabel.text = "Level " + a;
		foreach(Text txt in LevelTexts){
			txt.enabled = true;
			txt.text = //"レベル " + 
				a.ToString();
		}

		yield return new WaitForSeconds(2.0f);
		//Levellabel.enabled = false;
		foreach (Text txt in LevelTexts) {
			txt.enabled = false;
		}

		foreach(Text txt in LevelString){
			txt.enabled = false;
		}

		StopCoroutine("ShowLevel");

	}



	void ProcessAuthentication (bool success) {
		if (success) {
			Debug.Log ("Authenticated, checking achievements");
			
			// Request loaded achievements, and register a callback for processing them
			Social.ShowLeaderboardUI();
		}
		else
			Debug.Log ("Failed to authenticate");
	}

	void ReportScore (long score, string leaderboardID) {
		Debug.Log ("Reporting score " + score + " on leaderboard " + leaderboardID);
		Social.ReportScore (score, leaderboardID, success => {
			Debug.Log(success ? "Reported score successfully" : "Failed to report score");
		});
	}

	void PostTweet()
	{
		
	}

    public Texture2D shareTexture;

    public void ShareTwitter()
    {
        int score = Score;
        string url = "http://hautecouture.jp/";

		#if UNITY_ANDROID
        url = "https://play.google.com/store/apps/details?id=com.hautecouture.drutimber&hl=ja";
		#elif UNITY_IOS
		url = "https://itunes.apple.com/us/app/doruagano-ta-guai-wu-tato/id1144861135?l=ja&ls=1&mt=8";
#endif

        UM_ShareUtility.TwitterShare(
            "ドルアーガの塔 -怪物塔と黄金騎士-でモンスターを" + score
			+ "体撃破！君はどこまでモンスタータワーを切り崩せるかな？\n" + url + " #ドルアーガの塔"
			, shareTexture
			);

    }

    public void ShareLINE()
    {
		string storeURL = "";
		#if UNITY_ANDROID
        storeURL = "https://play.google.com/store/apps/details?id=com.hautecouture.drutimber&hl=ja";
		#elif UNITY_IOS
		storeURL = "https://itunes.apple.com/us/app/doruagano-ta-guai-wu-tato/id1144861135?l=ja&ls=1&mt=8";
#endif

        int score = Score;
        string msg = "ドルアーガの塔 -怪物塔と黄金騎士-でモンスターを" + score + "体撃破！君はどこまでモンスタータワーを切り崩せるかな？\n"
			+ storeURL;
        string url = "http://line.me/R/msg/text/?" + //WWW.EscapeURL(msg)
			System.Uri.EscapeUriString(msg);
        Application.OpenURL(url);
    }

    public void ShareFacebook()
    {
        if (!FB.IsInitialized)
        {
            FB.Init(Login, null, null);
            return;
        }
        else
        {
            Login();
        }
    }

    void Login()
    {
        if (!FB.IsLoggedIn)
        {
            FB.Login("", LoginCallback);
            return;
        }
        else
        {
            LoginCallback();
        }
    }

    string FeedLink = "http://hautecouture.jp/";
    string FeedLinkName = "";
    string FeedLinkDescription = "";
    string FeedPicture = "";
    private Dictionary<string, string[]> FeedProperties = new Dictionary<string, string[]>();

    void LoginCallback(FBResult result = null)
    {
        if (FB.IsLoggedIn)
        {
			#if UNITY_ANDROID
            FeedLink = "https://play.google.com/store/apps/details?id=com.hautecouture.drutimber&hl=ja";
			#elif UNITY_IOS
			FeedLink = "https://itunes.apple.com/us/app/doruagano-ta-guai-wu-tato/id1144861135?l=ja&ls=1&mt=8";
#endif

            int score = Score;

			FeedLinkName = "ドルアーガの塔 -怪物塔と黄金騎士-";
            FeedLinkDescription = "ドルアーガの塔 -怪物塔と黄金騎士- でモンスターを" + score
				+ "体撃破！君はどこまでモンスタータワーを切り崩せるかな？";

            FB.Feed(
                link: FeedLink,
                linkName: FeedLinkName,
                linkDescription: FeedLinkDescription,
                picture: "http://100apps.s3.amazonaws.com/000_Original/ShareImage/DDheader.jpg",
                properties: FeedProperties
            );
        }
    }
}
