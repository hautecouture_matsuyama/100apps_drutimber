﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;

public class TowerController : MonoBehaviour {

	public float heightMax = 50f;
	public float heightFixedCnst = 1.28f;
	public GameObject[] EnemyPrefabs;

	//public string[] enemyTags;

	public int slimeCount = 3;

	public float destroyTime = 3f;

	public GameObject enemyPrefabParent;

	public GameObject HitEffect;
	public int HitEffectOrder;
	public float effectDuration = 1f;

	//Queue SoccerBalls;
	List<GameObject> Enemies;

	// Use this for initialization
	void Start () {
		InitHamsters ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void InitHamsters()
	{
		SortedList<float, GameObject> SortedSoccerBalls = 
			new SortedList<float, GameObject> ();
		
		GameObject[] balls = GameObject.FindGameObjectsWithTag ("SoccerBall");
		
		foreach(GameObject go in balls){
			SortedSoccerBalls.Add(go.transform.position.y, go);
		}
		
		//SoccerBalls = new Queue ();
		Enemies = new List<GameObject> ();
		
		for(int i=0; i<SortedSoccerBalls.Count; i++){
			float key = SortedSoccerBalls.Keys[i];
			
			int spriteIndex = (i == 0)? Random.Range(0, slimeCount): 
				Random.Range(0, EnemyPrefabs.Length);
			
			GameObject goEnemy = 
				Instantiate(EnemyPrefabs[spriteIndex]) as GameObject;
			
			goEnemy.transform.parent = SortedSoccerBalls[key].transform;
			
			goEnemy.transform.localPosition = Vector3.zero;
			
			goEnemy.transform.localScale = new Vector3(1, 1, 1);

			//SoccerBalls.Enqueue(SortedSoccerBalls[key]);
			Enemies.Add(SortedSoccerBalls[key]);
		}
	}

	//dir -- ボールを蹴る方向（1,-1）
	public IEnumerator KickSoccerBall(int dir, int waitCount)
	{
		int counter = 0;

		while(counter < waitCount){
			counter++;
			yield return null;
		}

		GameObject hamGo// = /(GameObject)SoccerBalls.Dequeue ()
			;

		int index = GetLowItemIndex (Enemies);
		hamGo = Enemies[index];
		Enemies.RemoveAt (index);

		Animator enemyAnim = hamGo.GetComponentInChildren<Animator>();

		hamGo.GetComponent<Collider2D>().enabled = false;

		hamGo.GetComponent<Rigidbody2D>().gravityScale = 1;

		hamGo.GetComponent<Rigidbody2D>().fixedAngle = false;

		hamGo.GetComponent<Rigidbody2D>().AddForce(new Vector3(dir*300,Random.Range(0, 300),0));

		hamGo.GetComponent<Rigidbody2D>().AddTorque(Random.Range (-300, 300));

		GameObject hitEffectObject = Instantiate(
			HitEffect, hamGo.transform.position, 
			Quaternion.Euler(0, 0, 360*Random.value)) as GameObject;
		foreach(ParticleSystem ps in hitEffectObject
		        .GetComponentsInChildren<ParticleSystem>())
		{
			ps.GetComponent<Renderer>().sortingOrder = HitEffectOrder;
			
		}

		Destroy (hitEffectObject, effectDuration);

		enemyAnim.SetTrigger ("Damage");

		float maxHeight = hamGo.transform.position.y;
		foreach(GameObject go in Enemies//SoccerBalls
		        ){
			if(go.transform.position.y > maxHeight){
				maxHeight = go.transform.position.y;
			}
		}

		GameObject newEnemy = 
			Instantiate (enemyPrefabParent, 
			             Vector3.zero, 
			             Quaternion.identity)
				as GameObject;

		newEnemy.transform.parent = transform;

		newEnemy.transform.localPosition = 
			Mathf.Min(maxHeight + heightFixedCnst, heightMax) * Vector3.up;

		int spriteIndex = Random.Range(0, EnemyPrefabs.Length);
		
		GameObject goEnemyChild = 
			Instantiate(EnemyPrefabs[spriteIndex]) as GameObject;

		goEnemyChild.transform.parent = newEnemy.transform;
		
		goEnemyChild.transform.localPosition = Vector3.zero;
		
		goEnemyChild.transform.localScale = new Vector3(1, 1, 1);
		
		//SoccerBalls.Enqueue(newEnemy);
		Enemies.Add(newEnemy);

		Destroy (hamGo, destroyTime);

	}

	int GetLowItemIndex(List<GameObject> go)
	{
		float minHeight = go[0].transform.position.y;
		int minHeightIndex = 0;

		for(int i=0; i<go.Count; i++)
		{
			if(go[i].transform.position.y < minHeight){
				minHeightIndex = i;
				minHeight = go[i].transform.position.y;
			}
		}

		return minHeightIndex;
	}

	public void GameOverOperation(float tweenTime)
	{
		GameObject[] balls = GameObject.FindGameObjectsWithTag ("SoccerBall");

		foreach(GameObject go in balls)
		{
			SpriteRenderer sprn = go.GetComponentInChildren<SpriteRenderer>();

			sprn.DOColor(new Color(sprn.color.r,sprn.color.g,sprn.color.b,0), tweenTime);
		}
	}
}
