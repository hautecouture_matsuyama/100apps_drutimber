﻿using UnityEngine;
using System.Collections;

public class ScoreAnimScript : MonoBehaviour {

	public gameOver gover;

	public Animator animCtrl;

	/*private Animator animCtrl
	{
		get{
			if(_animCtrl == null){
				_animCtrl = gameObject
					.GetComponent<Animator>();
			}
			return _animCtrl;
		}
	}
	private Animator _animCtrl;
	*/

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	//パネルのアニメーションの後に実行されるので、
	//更新後のハイスコアを取得する
	public void bestScore()
	{
		if(gover.isScoreUpdated){
			//スコア更新
			
			animCtrl.Play("ShowBestScore");
		}
		/*
		if (PlayerPrefs.GetInt ("Score") > PlayerController.Score) {

		} else {
			//スコア更新

			animCtrl.Play("ShowBestScore");
		}*/
	}
}
