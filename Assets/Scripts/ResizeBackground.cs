﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class ResizeBackground : MonoBehaviour {

	//[SerializeField]
	//SpriteRenderer background;

	[SerializeField]
	GameObject[] x_scaled_objects; 

	[SerializeField]
	int orgScrWidth;

	[SerializeField]
	int orgScrHeight;

	[SerializeField]
	SpriteRenderer[] bg_black;
	[SerializeField]
	SpriteRenderer bg_black2;
	[SerializeField]
	SpriteRenderer[] candleLights;
	[SerializeField]
	float alpha_black;

	[SerializeField]
	HealthBar healthBar;

	[SerializeField]
	SpriteRenderer backgroundSprn;
	[SerializeField]
	bool test;

	float value = 1f;

	// Use this for initialization
	void Start () {
		float scale = 1;

		if (orgScrHeight / (float)orgScrWidth > 
			Screen.height / (float)Screen.width) {

			scale = orgScrHeight * Screen.width / 
				(float)(orgScrWidth * Screen.height);
		} else {

			scale = orgScrWidth * Screen.height / 
				(float)(orgScrHeight * Screen.width);
		}
		/*
		background.transform.localScale = 
			new Vector3 (background.transform.localScale.x * scale, 
			             background.transform.localScale.y * scale, 
			             background.transform.localScale.z);
		*/
		foreach(GameObject go in x_scaled_objects)
		{
			if(go != null){

				go.transform.localScale = 
					new Vector3 (go.transform.localScale.x * scale, 
					             go.transform.localScale.y, 
					             go.transform.localScale.z);

			}
		}
	}
	
	// Update is called once per frame
	void Update () {

		if(PlayerController.gameoverCheck == false){
			float gaugeRate = healthBar.health / healthBar.healthForLabel;

			for(int i=0; i<bg_black.Length; i++){
				bg_black[i].color = 
					new Color (bg_black[i].color.r,bg_black[i].color.g,bg_black[i].color.b,
				                            alpha_black * (1 - gaugeRate));
			}

			for(int i=0; i<candleLights.Length; i++){
				Color col = candleLights[i].color;
				
				float value = 1 - alpha_black * (1 - gaugeRate);
				
				candleLights[i].color = 
					new Color(value, value, value);
			}
		}

		if(test && backgroundSprn != null){

			switch(PlayerController.Score){
			case 10:
				value = 0.75f;
				break;
			case 20:
				value = 0.5f;
				break;
			case 30:
				value = 0.25f;
				break;
			case 40:
				value = 0f;
				break;
			}

			backgroundSprn.color = new Color(value, value, value);
		}

	}

	public void GameOverOperation(float tweenTime)
	{
		bg_black2.DOColor (new Color (bg_black2.color.r, bg_black2.color.g, bg_black2.color.b, 1), 
		                  tweenTime);
	}
}
