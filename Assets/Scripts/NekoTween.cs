﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class NekoTween : MonoBehaviour {

	[SerializeField]
	Vector3[] Positions;

	[SerializeField]
	float tweenTime = 0.75f;

	[SerializeField]
	int waitFrameCount = 5;

	Sprite waitingSprite;

	SpriteRenderer preSprn;

	// Use this for initialization
	void Start () {
		waitingSprite = transform.parent
			.GetComponent<PlayerController> ()
				.AnimationSprites[0];

		preSprn = gameObject.GetComponent<SpriteRenderer>();

		StartCoroutine(ToLeft ());

	}

	IEnumerator ToLeft()
	{
		//while(preSprn.sprite != waitingSprite){
		//	yield return null;
		//}

		int counter = 0;
		while(true){
			if(preSprn.sprite == waitingSprite){

				counter++;

			}else{
				counter = 0;
			}

			yield return null;

			if(counter >= waitFrameCount){
				break;
			}

		}

		transform.DOLocalMove(Positions[0],tweenTime)
			.OnComplete(ToRightStart);

	}
	void ToLeftStart()
	{
		StartCoroutine (ToLeft());
	}

	IEnumerator ToRight()
	{
		//while(preSprn.sprite != waitingSprite){
		//	yield return null;
		//}

		int counter = 0;
		while(true){
			if(preSprn.sprite == waitingSprite){

				counter++;

			}else{
				counter = 0;
			}

			yield return null;

			if(counter >= waitFrameCount){
				break;
			}

		}

		transform.DOLocalMove(Positions[1],tweenTime)
			.OnComplete(ToLeftStart);
	}
	void ToRightStart()
	{
		StartCoroutine (ToRight());
	}

	// Update is called once per frame
	void Update () {
		
	}
}
