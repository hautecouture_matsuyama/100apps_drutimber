﻿using UnityEngine;
using System.Collections;

public class Createbackground : MonoBehaviour {

	public float potionPercentage;
	public int potionInterval_percentage = 5;

	public int[] potionPartsIndex;
	public int[] normalPartsIndex;

	public int potionInterval = 10;
	public int intervalCount = 0;

	public GameObject lastgameobject;
	public GameObject[] ground;
	private GameObject lastchild;

	public bool enablePercentage = true;

	int noPotionCounter = 0;

	void Start(){

		intervalCount = 0;
	}

	int GetNextPartIndex(ref int usedIndex)
	{
		intervalCount++;

		bool condition = enablePercentage ? 
			Random.value < potionPercentage : intervalCount % potionInterval == 0;

		if (condition && noPotionCounter >= potionInterval_percentage
		    && GameManagerScript.potionFlag == false) {
			noPotionCounter = 0;

			//ポーションが降ってくる
			usedIndex = Random.Range(0, potionPartsIndex.Length);

			return potionPartsIndex[usedIndex];
		} else {
			noPotionCounter++;

			usedIndex = Random.Range(0, normalPartsIndex.Length);
			
			return normalPartsIndex[usedIndex];
		}
	}

	public void createchunk()
	{
		lastchild=lastgameobject;

		int usedIndex = 0;

		int a = //Random.Range(0,ground.Length)
			GetNextPartIndex(ref usedIndex);

		ground[a].SetActive(true);

		lastgameobject=ground[a];

		if(lastchild == lastgameobject)
		{
			/*
			if(a < ground.Length -1)
			{
			//	Debug.Log("last gameobject : " + a);
				a++;

				lastgameobject = ground[a];
			}
			else
			{
			//	Debug.Log("last  : " + a);
				//a--;	これでは偏る
				a = 0;
				lastgameobject = ground[a];
			}
			*/

			bool containPotion = false;
			foreach(int index in potionPartsIndex){
				if(index == a){
					containPotion = true;
				}
			}

			if(containPotion){
				a = potionPartsIndex[(usedIndex + 1) % potionPartsIndex.Length];
			}else{
				a = normalPartsIndex[(usedIndex + 1) % normalPartsIndex.Length];
			}

			lastgameobject=ground[a];

			lastgameobject.SetActive(true);

		}

		lastgameobject.transform.localPosition = new Vector3(0,lastchild.transform.localPosition.y+lastchild.transform.GetComponent<Renderer>().bounds.size.y,0);
		lastchild=lastgameobject;

		lastgameobject.GetComponent<BackParts> ().EnablePotions ();
	}
	
}
