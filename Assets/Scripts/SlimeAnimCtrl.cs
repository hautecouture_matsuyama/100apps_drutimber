﻿using UnityEngine;
using System.Collections;

public class SlimeAnimCtrl : BaseEnemyCtrl {

	[SerializeField]
	float jumpForce = 10;

	Rigidbody2D rigidBody2d{
		get{
			if(_rigidBody2d == null){
				_rigidBody2d = transform.parent.GetComponent<Rigidbody2D>();
			}
			return _rigidBody2d;
		}
	}
	Rigidbody2D _rigidBody2d;

	// Use this for initialization
	void Start () {
		base.Start ();

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void JumpActoin()
	{
		rigidBody2d.AddForce (Vector2.up * jumpForce, ForceMode2D.Impulse);
	}
}
