﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class shareScript : MonoBehaviour {

	public Image shareButton;
	//public Image retryButton;
	
	public Sprite[] shareButtonSprites;

	public Animator panelAnim;

	//public GameObject panelHide;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void shareOpenStart(){
		panelAnim.Play ("HideAnim1");
	}
	public void shareOpenComplete()
	{
		shareButton.sprite = shareButtonSprites[1];
		//panelHide.SetActive (true);
	}

	public void shareCloseStart()
	{
		panelAnim.Play ("HideAnim2");
	}
	public void shareCloseComplete()
	{
		shareButton.sprite = shareButtonSprites[0];
		//panelHide.SetActive (false);
	}
}
