﻿using UnityEngine;
using System.Collections;

public class GilParentScript : MonoBehaviour {

	public bool leftForced = false, rightForced = false;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if(col.tag == "turnleft"){
			leftForced = true;
		}else if(col.tag == "turnright"){
			rightForced = true;
		}
	}

	void OnTriggerExit2D(Collider2D col)
	{
		if(col.tag == "turnleft"){
			leftForced = false;
		}else if(col.tag == "turnright"){
			rightForced = false;
		}
	}
}
