﻿using UnityEngine;
using System.Collections;

public class MagicMotion : MonoBehaviour {

	public float speed;
	public Sprite[] animSprite;
	public float timeInterval;
	public float destroyTime;

	SpriteRenderer animSprn{
		get{
			if(_animSprn == null){
				_animSprn = gameObject.GetComponent<SpriteRenderer>();
			}
			return _animSprn;
		}
	}
	SpriteRenderer _animSprn;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Shot(int direction)
	{
		StartCoroutine (ShotCrtn(direction));

		StartCoroutine (AnimCrtn(direction));

		StartCoroutine (DestroyObject());

	}

	IEnumerator ShotCrtn(int direction)
	{
		while(true){
			transform.Translate (Vector3.right * direction * speed);
			
			yield return null;
		}

	}

	IEnumerator AnimCrtn(int direction)
	{
		if(direction < 0){
			Vector3 tmpScale = transform.localScale;
			tmpScale.x = -tmpScale.x;
			transform.localScale = tmpScale;
		}

		int index = 0;

		while(true)
		{
			animSprn.sprite = animSprite[index];

			yield return new WaitForSeconds(timeInterval);

			index = (index + 1) % animSprite.Length;
		}
	}

	IEnumerator DestroyObject()
	{
		yield return new WaitForSeconds (destroyTime);

		Destroy (gameObject);
	}

}
