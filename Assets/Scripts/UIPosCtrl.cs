﻿using UnityEngine;
using System.Collections;

public class UIPosCtrl : MonoBehaviour {

	//this is the ui element
	[SerializeField]
	Camera mainCam;
	[SerializeField]
	RectTransform CanvasRect;
	[SerializeField]
	public RectTransform UI_Element;
	[SerializeField]
	Vector2 differenceVec;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		SetUIAnchoredPosition ();
	}

	public void SetUIAnchoredPosition()
	{
		//this is your object that you want to have the UI element hovering over
		GameObject WorldObject = gameObject;
		

		//first you need the RectTransform component of your canvas
		//RectTransform CanvasRect = Canvas.GetComponent<RectTransform>();
		
		//then you calculate the position of the UI element
		//0,0 for the canvas is at the center of the screen, whereas WorldToViewPortPoint treats the lower left corner as 0,0. Because of this, you need to subtract the height / width of the canvas * 0.5 to get the correct position.
		
		Vector2 ViewportPosition = mainCam.WorldToViewportPoint(WorldObject.transform.position);
		Vector2 WorldObject_ScreenPosition = new Vector2(
			((ViewportPosition.x*CanvasRect.sizeDelta.x)-(CanvasRect.sizeDelta.x*0.5f)),
			((ViewportPosition.y*CanvasRect.sizeDelta.y)-(CanvasRect.sizeDelta.y*0.5f)));
		
		//now you can set the position of the ui element
		UI_Element.anchoredPosition = WorldObject_ScreenPosition + differenceVec;
	}


}
