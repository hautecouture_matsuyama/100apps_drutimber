﻿using UnityEngine;
using System.Collections;

public class MirrorKnightAnimCtrl : BaseEnemyCtrl {

	[SerializeField]
	AudioClip beforeAttackSE;
	[SerializeField]
	AudioClip afterAttackSE;

	// Use this for initialization
	void Start () {
		base.Start ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SetBeforeAttackSE()
	{
		soundSE.clip = beforeAttackSE;
	}

	public void SetAfterAttackSE()
	{
		soundSE.clip = afterAttackSE;
	}
}
