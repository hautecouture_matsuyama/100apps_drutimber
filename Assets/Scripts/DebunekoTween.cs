﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class DebunekoTween : MonoBehaviour {

	[SerializeField]
	bool isRotate = false;
	[SerializeField]
	Vector3 rot1 = new Vector3(0, 0, -2f);
	[SerializeField]
	Vector3 rot2 = new Vector3(0, 0, 2f);

	[SerializeField]
	bool isScaled = true;
	[SerializeField]
	Vector3 scale1 = new Vector3(1.2f, 0.8f, 1f);
	[SerializeField]
	Vector3 scale2 = new Vector3(0.8f, 1.2f, 1f);
	[SerializeField]
	float timeInterval = 2f;
	[SerializeField]
	Ease easeType = Ease.InOutSine;

	//Vector3 initPos;

	// Use this for initialization
	void Start () {
		//initPos = transform.position;

		if (isRotate) {
			Invoke (((System.Action)ToLeft).Method.Name, 
		        timeInterval * Random.value);
		}

		if (isScaled) {
			Invoke (((System.Action)XScale).Method.Name, 
		        timeInterval * Random.value);
		}
	}

	void ToLeft()
	{
		transform.DOLocalRotate (rot1, timeInterval)
			.SetEase(easeType).OnComplete(ToRight);

	}

	void ToRight()
	{
		transform.DOLocalRotate (rot2, timeInterval)
			.SetEase(easeType).OnComplete(ToLeft);
	}

	void XScale()
	{
		transform.DOScale (scale1, timeInterval)
			.SetEase (easeType).OnComplete (YScale);
	}

	void YScale()
	{
		transform.DOScale (scale2, timeInterval)
			.SetEase (easeType).OnComplete (XScale);
	}
	
	// Update is called once per frame
	void Update () {
	

	}
}
